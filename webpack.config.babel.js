import config from './project.json'
import webpack from 'webpack'
import path from 'path'
import HtmlWebPackPlugin from 'html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
const TerserPlugin = require('terser-webpack-plugin')

const PUBLIC_URL = config.url

module.exports = (env, args) => ({
  devtool: args.mode === 'production' ? false : 'source-map',

  mode: args.mode,

  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'src'),
      path.resolve(__dirname, ''),
    ],
  },

  entry: {
    index: 'index.js',
  },

  output: {
    chunkFilename: '[name].js',
    filename: '[name].js',
    publicPath: args.mode === 'production' ? PUBLIC_URL : '/',
  },

  devServer: {
    historyApiFallback: true,
  },

  optimization: {
    minimize: args.mode === 'production',
    minimizer:
      args.mode === 'production'
        ? [
            new TerserPlugin({
              terserOptions: {
                format: {
                  comments: false,
                },
              },
              extractComments: false,
            }),
          ]
        : [],
  },

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules(?!\/ansi-regex)(?!\/strip-ansi)/,
        use: {
          loader: 'babel-loader',
        },
      },

      {
        test: /\.css$/,
        exclude: /module\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: 'global',
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: args.mode === 'development',
            },
          },
        ],
      },

      {
        test: /module\.css$/,
        include: path.resolve(__dirname, '../'),

        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              url: false,
              modules: {
                mode: 'local',
                context: path.resolve(__dirname, 'src'),
                hashPrefix: Math.random().toString(32),
                localIdentName: '[local]--[hash:base64:5]',
              },
            },
          },
          {
            loader: 'postcss-loader',
          },
        ],
      },

      {
        test: /\.svg$/i,
        use: [
          {
            loader: 'raw-loader',
          },
          {
            loader: 'svgo-loader',
            options: {
              configFile: false,
            },
          },
        ],
      },

      {
        exclude: /\.(m?js|css|html|json|svg|jpe?g|gif|png)$/i,
        loader: 'file-loader',
        options: { name: '[name].[hash:4].[ext]' },
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      DEBUG: args.mode === 'development',
      PUBLIC_URL: JSON.stringify(args.mode === 'production' ? PUBLIC_URL : ''),
    }),

    new HtmlWebPackPlugin({
      id: config.name,
      title: config.title,
      template: './src/index.html',
      filename: './index.html',
      chunks: ['index'],
    }),

    new CopyWebpackPlugin({
      patterns: [
        {
          context: __dirname,
          from: 'static',
          to: '',
        },
      ],
    }),
  ],
})
