import React from 'react'
import { createRoot } from 'react-dom/client'
import App from 'App'
import config from 'project.json'

createRoot(document.querySelector(`#${config.name}`)).render(<App />)
