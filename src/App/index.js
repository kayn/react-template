import React from 'react'
import S from './module.css'
export default class App extends React.Component {
  render() {
    return <div className={S.app}>React App</div>
  }
}
